package dam.android.alex.u5t9httpclient;


import static android.widget.Toast.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final static String URL_GEONAMES = "http://api.geonames.org/wikipediaSearchJSON";
    private final static String USER_NAME = "alexlopez";
    private final static String API_KEY ="ac501dcb84f91b271f3715b884b00f3e";
    private final static int ROWS = 10;

    private EditText etPlaceName;
    private Button btSearch;
    private ListView lvSearchResult;
    private ArrayList<GeonamesPlace> listSearchResult;
    private ExecutorService executor;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        progressBar=findViewById(R.id.indeterminate_circular_indicator);
        etPlaceName = findViewById(R.id.etPlaceName);
        btSearch = findViewById(R.id.btSearch);
        btSearch.setOnClickListener(this);
        listSearchResult = new ArrayList<>();
        lvSearchResult = findViewById(R.id.lvSearchResult);
        lvSearchResult.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listSearchResult));
        progressBar.setVisibility(View.INVISIBLE);
        lvSearchResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView, View view, int i, long l) {//TODO Creamos un on item click listener para que cuando pulsamos sobre la list view se vean los datos que yo quiero en el toast
                URL url;
                try {


                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme("http")
                            .authority("api.openweathermap.org")
                            .appendPath("/data/2.5/weather")
                            .appendQueryParameter("units","metric")
                            .appendQueryParameter("lat", listSearchResult.get(i).getLatitud())
                            .appendQueryParameter("lon", listSearchResult.get(i).getLongitud())
                            .appendQueryParameter("appid",API_KEY);
                    url = new URL(builder.build().toString());
                    startBackgroundTarkWeather(url,listSearchResult.get(i));

                } catch (MalformedURLException e) {
                    Log.i("URL", e.getMessage());
                }
            }

        });





    }



    private Boolean isNetworkAvailabre() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network nw = connectivityManager.getActiveNetwork();
            if (nw == null) return false;
            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);
            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR));
        } else {
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isConnected();
        }
    }

    @Override
    public void onClick(View v) {
        if (isNetworkAvailabre()) {
            String place = etPlaceName.getText().toString();
            if (!place.isEmpty()) {

                URL url;
                try {

                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme("http")
                            .authority("api.geonames.org")
                            .appendPath("wikipediaSearchJSON")
                            .appendQueryParameter("q", place)
                            .appendQueryParameter("maxRows", String.valueOf(ROWS))
                            .appendQueryParameter("username", USER_NAME)
                            .appendQueryParameter("lang","ES");



                    url = new URL(builder.build().toString());
                    System.out.println(url);

                    startBackgroundTark(url);
                    progressBar.setVisibility(View.VISIBLE);


                } catch (MalformedURLException e) {
                    Log.i("URL", e.getMessage());
                }
            } else
                makeText(this, "Write a place to search", LENGTH_SHORT).show();


        } else
            makeText(this, "Sorry,network is not availabre", LENGTH_SHORT).show();
    }



    private void startBackgroundTark(URL url) {
        final int CONNECTION_TIMEOUT=10000;
        final  int READ_TIMEOUT=7000;
        executor= Executors.newSingleThreadExecutor();
        Handler handler=new Handler(Looper.getMainLooper());
        executor.execute(new Runnable() {
            @Override
            public void run() {

                HttpURLConnection urlConnection=null;
                final ArrayList<GeonamesPlace> searchResult=new ArrayList<>();
                try {
                    urlConnection =(HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);
                    urlConnection.connect();
                    getData(urlConnection,searchResult);
                } catch (IOException e){
                    Log.i("IOException",e.getMessage());

                }catch (JSONException e){
                    Log.i("JSONException",e.getMessage());

                }finally {
                    if (urlConnection!=null) urlConnection.disconnect();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (searchResult.size()>0){
                            ArrayAdapter<GeonamesPlace> adapter=(ArrayAdapter<GeonamesPlace>) lvSearchResult.getAdapter();
                            adapter.clear();
                            adapter.addAll(searchResult);
                            adapter.notifyDataSetChanged();
                        }else
                            makeText(getApplicationContext(), "Not possible to contact"+URL_GEONAMES, LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void getData(HttpURLConnection urlConnection, ArrayList<GeonamesPlace> searchResult)
        throws IOException,JSONException{
        if (urlConnection.getResponseCode()== HttpURLConnection.HTTP_OK){
            String resultStream= readStream(urlConnection.getInputStream());
            JSONObject json=new JSONObject(resultStream);
            JSONArray jsonArray= json.getJSONArray("geonames");

            if (jsonArray.length()>0){
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject item=jsonArray.getJSONObject(i);
                    listSearchResult.add(new GeonamesPlace(item.getString("summary"), item.getString("lat"),item.getString("lng")));
                    searchResult.add(listSearchResult.get(i));

                }
                listSearchResult.clear();
                progressBar.setVisibility(View.INVISIBLE);
            }else searchResult.add(new GeonamesPlace("No information found at geonames","",""));
        }else {
            Log.i("URL","ErrorCode: "+urlConnection.getResponseCode());
        }


        if (this.getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
    }




    private void startBackgroundTarkWeather(URL url,GeonamesPlace geonamesPlace) {
        //Todo creamos otro startBackGround para utilizarlo solamente para el toast
        final int CONNECTION_TIMEOUT=10000;
        final  int READ_TIMEOUT=7000;
        executor= Executors.newSingleThreadExecutor();
        Handler handler=new Handler(Looper.getMainLooper());
        executor.execute(new Runnable() {
            @Override
            public void run() {

                HttpURLConnection urlConnection=null;

                try {
                    urlConnection =(HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);
                    urlConnection.connect();
                    getDataWeather(urlConnection,geonamesPlace);

                } catch (IOException e){
                    Log.i("IOException",e.getMessage());

                }catch (JSONException e){
                    Log.i("JSONException",e.getMessage());

                }finally {
                    if (urlConnection!=null) urlConnection.disconnect();
                }

            }
        });

    }



    private void getDataWeather(HttpURLConnection urlConnection, GeonamesPlace geonamesPlace)
            throws IOException,JSONException{
        if (urlConnection.getResponseCode()== HttpURLConnection.HTTP_OK){

            //Todo hacemos la consulta a la api para obtener los datos que necesitamos  y asignarselos a la clase GeonamesPlace
            String resultStream= readStream(urlConnection.getInputStream());
            JSONObject json=new JSONObject(resultStream);
            JSONArray jsonArray=json.getJSONArray("weather");
            JSONObject jsonObject=json.getJSONObject("main");
            geonamesPlace.setClouds(jsonArray.getJSONObject(0).getString("description"));
            geonamesPlace.setHumidity(jsonObject.getString("humidity"));
            geonamesPlace.setTemp(jsonObject.getString("temp"));

            Thread thread = new Thread(() -> runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    makeText(getApplicationContext(), "Weather conditions for {" + geonamesPlace.getLatitud() + "," + geonamesPlace.getLatitud() + "}\nTEMP:"+geonamesPlace.getTemp()+"C\nHUMDITY: " +geonamesPlace.getHumidity()+ "%\n"+geonamesPlace.getClouds(), LENGTH_LONG).show();
                }
            }));
            thread.start();
            //Todo creo un thread para que me ejecute el toast con los datos que quiero tener en el toast



        }else {
            Log.i("URL","ErrorCode: "+urlConnection.getResponseCode());
        }


        if (this.getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
    }





    private String readStream(InputStream inputStream)throws IOException {
        StringBuilder sb=new StringBuilder();
        BufferedReader reader=new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));

        String nextLine="";
        while ((nextLine=reader.readLine())!=null){
            sb.append(nextLine);
        }
        return sb.toString();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (executor!=null){
            executor.shutdown();
            Log.i("EXECUTOR","ALL TASKS CANCELLED!!!!!");
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        //TODO CREAMOS EL CICLO DE VIDA
        super.onRestoreInstanceState(savedInstanceState);
        listSearchResult.addAll((Collection<? extends GeonamesPlace>) savedInstanceState.getSerializable("save"));
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        //TODO CREAMOS EL CICLO DE VIDA
        super.onSaveInstanceState(outState);
        outState.putSerializable("save",listSearchResult);

    }




}