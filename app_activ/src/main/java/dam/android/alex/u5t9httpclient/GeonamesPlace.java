package dam.android.alex.u5t9httpclient;

public class GeonamesPlace {
    private String latitud;
    private String longitud;
    private String summary;
    private String temp;
    private String humidity;
    private String clouds;

    public GeonamesPlace() {
    }

    public GeonamesPlace(String summary,String latitud, String longitud) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.summary=summary;
    }
    public GeonamesPlace(String summary){
        this.summary=summary;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getSummary() {
        return summary;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getClouds() {
        return clouds;
    }

    public void setClouds(String clouds) {
        this.clouds = clouds;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Override
    public String toString() {
        return  summary+"\nLatitud: "+latitud+"\t Longitud: "+longitud;
    }
}
